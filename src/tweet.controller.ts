import { Body, Controller, Delete, Get, Param, Post, Put, Patch, Inject } from '@nestjs/common';
import { TweetService } from './tweet.service';
import { Client, ClientProxy, MessagePattern, Transport } from '@nestjs/microservices';
import {getRepository, getManager} from "typeorm";
import { Tweet, TweetGetAll, TweetGetByProfileIdDTO, TweetUpdateDTO } from './Entity/Tweet';
import BadRequestException from './exceptions/badRequest.exception';
import { read } from 'fs/promises';
import { Observable } from 'rxjs';

@Controller()
export class TweetController {
  constructor(private readonly TweetService: TweetService, @Inject('TWEET_SERVICE')  private readonly client: ClientProxy) {
  }

  

  @Get('/tweet/:id')
  async getTweetById(@Param('id') tweetId: string){
    //return 
    // return this.TweetService.getAll();
  }

  @Get('/tweets/:pageNumber')
  async getAll(@Param('pageNumber') pageNumber: number){
    //return 
    return this.TweetService.getAll(pageNumber);
  }

  @Get('/getbytrend/:trend')
  async getByTrend(@Param('trend') trendname: string){
    return this.TweetService.getByTrend(trendname);
  }

  @Get('/tweets/:profileId/:pageNumber')
  async getByTweetId(@Param('pageNumber') pageNumber: number, @Param('profileId') profileId: string){
    return this.TweetService.getByProfileId(pageNumber, profileId);
  }

  @Patch('/tweet/update')
  async update(@Body() tweet: TweetUpdateDTO){
    const res_tweet: Tweet = await this.TweetService.getById(tweet.id);
    res_tweet.id = tweet.id;
    if (!res_tweet) {
      throw new BadRequestException(
        `Project with id ${tweet.id} does not exist... (ಥ﹏ಥ)`,
      );
    }

    Object.keys(tweet).forEach((key) => {
      if (!res_tweet.hasOwnProperty(key)) {
        throw new BadRequestException(
          `Property ${key} does not exist on type Project...`,
        );
      } else {
        res_tweet[key] = tweet[key];
      }
    });

    return await this.TweetService.update(res_tweet);
  }

  @Patch('/like')
  async Like(@Body() tweet: TweetUpdateDTO){
    const res_tweet: Tweet = await this.TweetService.getById(tweet.id);
    res_tweet.likes++;
    return await this.TweetService.update(res_tweet);
  }

  @Post('/tweet')
  async create(@Body() tweet: Tweet){
    console.log(tweet.trend);
    this.client.send<string[]>('tweet_created', tweet.trend);
    let dateTime = new Date();
    tweet.datetime = dateTime;
    return await this.TweetService.create(tweet);
  }

  @Delete('/tweet/delete/:id')
  async delete(@Param("id") tweetId: number){
    return await this.TweetService.delete(tweetId);
  }
}
