import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TweetController } from './tweet.controller';
import { TweetService } from './tweet.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tweet } from './Entity/Tweet';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'TWEET_SERVICE',
        transport: Transport.REDIS,
        options: {
          url: 'redis://localhost:6379',
          auth_pass: 'redispassword'
        }
      },
    ]),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '178.84.152.77',
      port: 3308,
      username: 'root',
      password: 'wachtwoord',
      database: 'Tweet',
      entities: [Tweet],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Tweet])],
  controllers: [TweetController],
  providers: [TweetService],
})
export class AppModule { }
