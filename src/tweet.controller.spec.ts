import { ClientsModule, Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { Tweet } from './Entity/Tweet';
import { TweetController } from './tweet.controller';
import { TweetService } from './tweet.service';

describe('TweetController', () => {
  let tweetController: TweetController;
  let tweetService: TweetService

  beforeEach(async () => {
    tweetService = new TweetService(new Repository<Tweet>());

    const app: TestingModule = await Test.createTestingModule({
      providers: [TweetService],
    }).overrideProvider(TweetService)
      .useValue(tweetService)
      .compile();
  });

  describe('Test', () => {
    it('test test', async () => {
      const test = 'test'
      const test_two = 'test'

      expect(test).toBe(test_two);
    });
  });
});
