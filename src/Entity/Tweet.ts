import { Entity, PrimaryGeneratedColumn, Column, Generated } from "typeorm";

@Entity()
export class Tweet {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    body!: string;

    @Column({default: 0})
    likes: number;

    @Column('datetime')
    datetime!: Date;

    @Column()
    profileId!: string;

    @Column("simple-array")
    trend: string[];

    @Column("simple-array")
    mention: string[];
}

export class TweetGetByProfileIdDTO{
    profileId: string;
    pageNumber: number;
}

export class TweetUpdateDTO{
    id: string;
    body: string;
}

export class TweetGetAll{
    pageNumber: number;
}


