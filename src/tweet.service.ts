import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {getRepository, getManager, Repository, Like} from "typeorm";
import { Tweet, TweetGetByProfileIdDTO } from './Entity/Tweet';

@Injectable()
export class TweetService {

  constructor(
    @InjectRepository(Tweet)
    private tweetRepository: Repository<Tweet>,
  ) {}

  async getById(tweetId: string): Promise<Tweet> {
    return this.tweetRepository.findOne(tweetId);
  }

  async getByTrend(trendname: string){
    //TO DO magische database dingen.
    const take = 10;
    const skip = 0;

    console.log(trendname);

    const [result, total] = await this.tweetRepository.findAndCount(
      {
          take: take,
          skip: skip,
          where: { trend: Like('%' + trendname + '%') }
      });

    return {data: result, count: total};
  }
  async create(tweet: Tweet): Promise<Tweet> {
    return this.tweetRepository.save(tweet);
  }

  async getAll(pageNumber: number){
    //TO DO magische database dingen.
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.tweetRepository.findAndCount(
      {
          take: take,
          skip: skip,
          order: {datetime: 'DESC'}
      });

    return {data: result, count: total};
  }

  async getByProfileId(pageNumber: number, profileId: string){
    //TO DO magische database dingen.
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.tweetRepository.findAndCount(
      {
        where: { profileId: Like('%' + profileId + '%') },
        take: take,
        skip: skip,
        order: {datetime: 'DESC'}
      }
  );

    return {data: result, count: total};
  }

  async update(tweet: Tweet): Promise<String> {
    this.tweetRepository.update(tweet.id, tweet);
    return 'Tweet updated';
  }

  async delete(tweetid: number){
    this.tweetRepository.delete(tweetid);
    return 'Tweet deleted';
  }

}
